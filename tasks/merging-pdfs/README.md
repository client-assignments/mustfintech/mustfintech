# Merging PDFs

## Description

- Shows how can multiple PDFs can be merged to gather and get single PDF file.

## Goals

- Multiple PDFs should get merged.
- Generate single PDF file.

## How to use

1. Go to the task directory in terminal.
   ```bash
   $ cd tasks/merging-pdfs
   ```
2. Run command `npm i` to install dependencies. (first time only)
3. Go to the file [src/index.js](src/index.js) and change the variables as your
   preference.
   - **Note:** Description of variables has been given in this file.
4. Now, Run command `npm start` to start the merging process.
