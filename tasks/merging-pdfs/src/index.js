const PDFMerger = require('pdf-merger-js');

/**
 * Use below variables to set inputs
 */

/**
 * The folder to take input PDFs and generate output PDF.
 * Add your input pdfs in this folder.
 *
 * NOTE:
 * - You can set whatever folder you like.
 * - Set path relative to task folder (merging-pdf) or full absolute path (C:\folder\to\pdfs).
 */
const PDFDir = 'assets/pdfs/';

/**
 * Add input names of PDF files.
 *
 * NOTE: File extension is required
 * - Invalid : 'Hello', 'hello.'
 * - Valid   : 'hello.pdf', 'welcome.pdf'
 */
const inputPDFNames = [
  'file1.pdf',
  'file2.pdf'
];

/**
 * Add output PDF file name
 *
 * NOTE: File extension is required
 * - Invalid : 'Hello', 'hello.'
 * - Valid   : 'hello.pdf', 'welcome.pdf'
 */
const outputPDFName = 'output.pdf';

/**
 * +-----------------------------+
 * | NOTHING TO CHANGE FROM HERE |
 * +-----------------------------+
 */

/**
 * Function used to merge the PDFs
 * @param {string} PDFDir PDF Dir to take input PDF and generate output PDF
 * @param {string[]} inputPDFNames Input PDF names (Extension is required)
 * @param {string} outputPDFName Output PDF names (Extension is required)
 * @return {Promise<void>}
 */
async function mergePDFs(PDFDir, inputPDFNames, outputPDFName) {
  console.log(`\n- Merging process started for ${inputPDFNames.length} PDFs`);
  const merger = new PDFMerger();
  for (let pdfName of inputPDFNames) {
    await merger.add(PDFDir + pdfName);
  }
  await merger.save(PDFDir + outputPDFName);
}

mergePDFs(PDFDir, inputPDFNames, outputPDFName).then(() => {
  console.log('- All the PDFs are merged.');
  console.log(`- Output PDF filepath is "${PDFDir + outputPDFName}".\n`);
});
