const { chromium } = require('playwright');

/**
 * Use below variables to set inputs
 */

// User Id for platform
const userId = 'alwls2488';

// Password for platform
const password = 'apxktmxk0!23';

// Address to search
const address = '경기도 고양시 일산동구 강석로 152 강촌마을아파트 제701동 제2층 제202호 [마두동 796]';

// Street Number for input
const streetNumber = '796';

/**
 * +-----------------------------+
 * | NOTHING TO CHANGE FROM HERE |
 * +-----------------------------+
 */

let browser;

async function main() {
  browser = await chromium.launch({ headless: false });
  const context = await browser.newContext();
  const page = await context.newPage();
  await page.goto('https://cloud.eais.go.kr/', {
    waitUntil: 'domcontentloaded'
  });
  await page.click('.btnLogin');
  await page.locator('input[name=membId]').fill(userId);
  await page.type('input[name=pwd]', password);
  await page.click(
    '#container > div.content.pb80 > div > div > div.fl > div.loginForm > button');
  await page.click(
    '#section1 > div > div.mainInner > div.registerUi > div.bldreDiv.bldre1 > a');
  await page.type(
    '#eleasticSearch > div > div > div.multiselect__tags > input',
    address
  );
  await page.click(
    '#container > div.content.clearFix > div > div.floatWarp.mt30.clearFix > div.contLeft > div.srchArchitecture > div.searchBuildingWarp > div.AddrSearch > button.btnLotNum');
  const dropdown = await page.$(
    '#container > div.content.clearFix > div > div.floatWarp.mt30.clearFix > div.contLeft > div.srchArchitecture > div.popArchitecture > div > div.formIn > select.wd19');
  await dropdown.selectOption({ value: '1083' });
  const dropDown2 = await page.$(
    '#container > div.content.clearFix > div > div.floatWarp.mt30.clearFix > div.contLeft > div.srchArchitecture > div.popArchitecture > div > div.formIn > select:nth-child(2)');
  await dropDown2.selectOption({ value: '41285' });
  const dropDown3 = await page.$(
    '#container > div.content.clearFix > div > div.floatWarp.mt30.clearFix > div.contLeft > div.srchArchitecture > div.popArchitecture > div > div.formIn > select:nth-child(3)');
  await dropDown3.selectOption({ value: '10500' });
  const dropDown4 = await page.$(
    '#container > div.content.clearFix > div > div.floatWarp.mt30.clearFix > div.contLeft > div.srchArchitecture > div.popArchitecture > div > div.formIn > select.wd15');
  await dropDown4.selectOption({ value: '0' });
  await page.type('input[name=mnnm]', streetNumber);
  await page.click(
    '#container > div.content.clearFix > div > div.floatWarp.mt30.clearFix > div.contLeft > div.srchArchitecture > div.popArchitecture > div > div.btnWarp.mt20 > button.btnNext.btnSolid.btnLarge.btn_dark');
  await page.locator('.st2').click();
  await page.locator('#ag-59-input').check();
  await page.click('#complaintToltal > button');
  await page.click(
    '#container > div.content.clearFix > div > div.floatWarp.mt30.clearFix > div.floatComplaint.contRight > button');
  await page.click(
    '#container > div.content > div > div.btns > button.btnNext.btnSolid.btnLarge.btn_blue');
  await page.locator(
      '#container > div.content > div > div.bbsWrap.mt40 > table > tbody > tr:nth-child(1) > td:nth-child(5) > a')
    .click();
}

main().then().finally(() => {
  setTimeout(async () => {
    await browser.close();
  }, 30000);
});
