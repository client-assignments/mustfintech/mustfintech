# Mustfintech

## Tasks

1. [Crawling RPA](tasks/crawling-rpa/README.md)
2. [Merging PDFs](tasks/merging-pdfs/README.md)
3. [Convert PDF Language](tasks/convert-pdf-language/README.md)
